<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\News;

class NewController extends Controller
{
  public function index() {
    $news = News::where(['cat' => 'news', 'visible' => '1'])->orderBy('id', 'desc')->limit(20)->get();
    foreach ($news as $new) {
      $new->date = date('d.m.Y', $new->date);
    }
    return view('pages.news', compact('news'));
  }
}
