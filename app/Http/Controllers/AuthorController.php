<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Author;

class AuthorController extends Controller
{
  public function posts_by_category() {

  }

  public function index() {
    $authors = Author::get();
    foreach ($authors as &$author) {
      if (empty($author->author_img)) {
        $author->author_img = '/img/_src/authors/no_photo.png';
      } else {
        $author->author_img = '/img/_src/articles/' . $author->author_img;
      }
    }
    return view('pages.authors', compact('authors'));
  }
}
