@extends('layouts.app')

@section('title', $page->title)
@section('keywords', $page->keywords)
@section('description', $page->description)

@section('content')
<div class="page">
  <div class="content">
    <h2 class="page_title">{{ $page->title }}</h2>
    {!! $page->content !!}
  </div>
</div>
@endsection
