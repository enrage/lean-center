<nav class="main_menu">
  <ul>
    <li><a href="/category/news">Новости Lean</a></li>
    <li><a href="/category/history">Бизнес-кейсы</a></li>
    <li><a href="/category/certification">Сертификация оптимизаторов</a></li>
    <li><a href="/category/articles">Статьи</a></li>
    <li><a href="/category/journal">Журнал Lean-center</a></li>
    <li><a href="/category/cpobp">ЦПОБП</a></li>
    <li><a href="/category/video">Видео</a></li>
    <li><a href="/authors">Авторы</a></li>
  </ul>
</nav>
