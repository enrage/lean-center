<aside class="sidebar">
  <div class="news_block">
    <h4 class="block_title">Новости Lean-center</h4>
    <div class="post_news">
      @foreach ($news as $new)
      <div class="post_new">
        <p class="date_new">{{ $new->date }}</p>
        <h3 class="title_new"><a href="/news/{{ $new->link }}" class="link_new">{{ $new->title }}</a></h3>
      </div>
      @endforeach
    </div>
    <div class="archive_news"><a href="/news_archive">Архив новостей</a></div>
  </div>

  <div class="banner_po">
    <a href="http://openadvisors.ru/page/our_software" target="_blank"><img src="/img/_src/banner_po.png" alt="Программное обеспечение для оптимизаторов"></a>
  </div>

  <div class="banner_sertificatcia">
    <a href="/category/certification"><img src="/img/_src/banner_sertificatсia.png" alt="Сертификация оптимизаторов"></a>
  </div>

  <div class="open_advisors">
    <a href="http://openadvisors.ru" target="_blank"><img src="/img/_src/openadvisors.png" alt="Open Advisors"></a>
  </div>
</aside>
