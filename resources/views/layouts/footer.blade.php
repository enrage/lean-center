<footer id="footer">
  <div class="container">
	<div class="top_footer">
	  <nav class="bottom_cat_menu">
		<h4 class="bottom_menu_title">Категории</h4>
		<ul>
		  <li><a href="/category/news">Новости Lean</a></li>
		  <li><a href="/category/history">Бизнес-кейсы</a></li>
		  <li><a href="/category/certification">Сертификация оптимизаторов</a></li>
		  <li><a href="/category/articles">Статьи</a></li>
		  <li><a href="/category/journal">Журнал Lean-center</a></li>
		  <li><a href="/category/cpobp">ЦПОБП</a></li>
		  <li><a href="/category/video">Видео</a></li>
		  <li><a href="/authors">Авторы</a></li>
		</ul>
	  </nav>
	  <nav class="bottom_main_menu">
		<h4 class="bottom_menu_title">Меню</h4>
		<ul>
		  <li><a href="/">Главная</a></li>
		  <li><a href="/about">О проекте</a></li>
		  <li><a href="/azbuka_lean">Азбука LEAN</a></li>
		  <li><a href="/for_authors">Авторам</a></li>
		  <li><a href="/for_partners">Партнерам</a></li>
		  <li><a href="/for_sponsors">Спонсорам</a></li>
		</ul>
	  </nav>
	  <div class="social_media">
		<h4 class="social_media_title">Социальные сети</h4>
		<ul>
		  <li><a href="#" class="vk"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
		  <li><a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		  <li><a href="#" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		  <li><a href="#" class="linked"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		  <li><a href="#" class="yt"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
		</ul>
	  </div>

	  <div class="search">
		<h4 class="search_title">Поиск</h4>
		<input type="text" name="search" id="search">
	  </div>
	</div>

	<div class="copyright">
	  <p class="conditions">Размещение статей, видеороликов и иных метариалов нашего портала на сторонних ресурсах
		возможно при однозначном указании
		источника</p>
	  <p class="copy">Copyright © Lean-Center 2015 - 2020</p>
	</div>
  </div>
</footer>
